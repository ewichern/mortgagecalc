from flask import Flask, render_template, request
from calc import mortgage_calc

app = Flask(__name__)

@app.route('/')
def index():
    return 'Index Page'

@app.route('/hello')
def hello_world():
    return 'Hello World!'

@app.route('/<path:user_path>')
def show_path(user_path):
    return 'Path: %s' % user_path

@app.route('/calc/', methods=['POST', 'GET'])
def calc():
    if request.method == 'POST':
        form_data = request.form.copy()

        mortgage_info = mortgage_calc(form_data)

        for key, value in mortgage_info.iteritems():
            form_data.add(key, round(value))

    else:
        form_data = {'home_value' : 300000.00, 'loan_amount' : 250000.00, 'interest_rate' : 4, 'loan_term' : 30, 'property_tax' : 1.25, 'PMI_percent' : 0.5 }

    return render_template('calc2.html', form_data = form_data)

if __name__ == '__main__':
    app.debug = True
    app.run()
