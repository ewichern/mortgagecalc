from math import log

def mortgage_calc(form_data):
    home_value = round(float(form_data['home_value']), 2)
    principle = round(float(form_data['loan_amount']), 2)
    rate = float(form_data['interest_rate']) / 12 / 100
    term = int(form_data['loan_term']) * 12
    tax = float(form_data['property_tax']) / 100
    pmi = float(form_data['PMI_percent']) / 100
    # print (principle)
    # print (rate)
    # print (term)
    # print (tax)
    # print (pmi)

    monthly_mortgage_payment = (rate * principle) / (1 - (1 + rate) ** (term * -1))
    # print(monthly_mortgage_payment)

    monthly_tax = (home_value * tax) / 12
    total_tax = monthly_tax * term
    # print(monthly_tax)

    monthly_payment = monthly_mortgage_payment + monthly_tax
    # print(monthly_payment)

    total_paid = monthly_payment * term
    total_no_tax = monthly_mortgage_payment * term
    total_interest = total_no_tax - principle

    pmi_payment = (principle * pmi) / 12
    # print(pmi_payment)

    eighty_percent_value = home_value * 0.80
    log_base = 1 + rate
    log_x = ((rate * eighty_percent_value) - monthly_mortgage_payment) / ((rate * principle) - monthly_mortgage_payment)

    months_of_PMI = round(log(log_x, log_base))

    total_PMI = months_of_PMI * pmi_payment

    payment_with_pmi = monthly_payment + pmi_payment

    return {'monthly_payment' : monthly_payment, 'monthly_mortgage_payment' : monthly_mortgage_payment, 'monthly_tax' : monthly_tax, 'eighty_percent_value' : eighty_percent_value, 'total_paid' : total_paid, 'total_interest' : total_interest, 'total_tax' : total_tax, 'total_pmi' : total_PMI, 'months_of_pmi' : months_of_PMI, 'pmi_payment' : pmi_payment, 'payment_with_pmi' : payment_with_pmi}
