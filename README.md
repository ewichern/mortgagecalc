# Readme

Basic Flask app for a mortgage calculator. No styling has been done yet!

### Setup

(all instructions following are for Linux/OSX command line)

* Assuming you have python and pip installed - install virtualenv if necessary with `pip install virtualenv`. 
* Clone the repo to an empty directory somewhere. Change to that directory and create a virtual environment with `virtualenv nameOfVirtualEnvironment`. 
* Now activate that environment with `. nameOfVirtualEnvironment/bin/activate` or `source nameOfVirtualEnvironment/bin/activate`. The command line should change to `(nameOfVirtualEnvironment)/parentDirectory/projectDirectory$`. 
* Now **finally** the dependencies for the project can be installed by entering `pip install -r requirements.txt` at the command line.

Run `python hello.py` and then navigate to `http://localhost:5000` in a web browser.